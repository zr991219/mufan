package com.hj.common.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
    public static SimpleDateFormat dataTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat yyyyMMddHHmmssDateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    public static SimpleDateFormat CurrentTimeFormat = new SimpleDateFormat("yyyyMMddHHmmsssss");
    public static SimpleDateFormat dateFormatChinese = new SimpleDateFormat("yyyy年MM月dd日");
    public static SimpleDateFormat dateTimeFormatChinese = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
    public static SimpleDateFormat dateMonthFormat = new SimpleDateFormat("yyyyMM");
    public static SimpleDateFormat dateMonthFormatYYYY_MM = new SimpleDateFormat("yyyy-MM");


    /**
     * 获取系统当前时间
     * @return
     */
    public static String getCurrentTime(){
        long time = System.currentTimeMillis();//当前时间毫秒值
        return dataTimeFormat.format(time);
    }


    //TODO : calender相关方法
    //指定日期减去指定天数的那一天
    public static Date diffDate(Date date, int day) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(getMillis(date) - ((long) day) * 24 * 3600 * 1000);
        return c.getTime();
    }

    //为指定日期增加指定天数
    public static Date addDays(Date date, int day) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, day);
        date = c.getTime();//获取当前时间
        return date;
    }

    //计算两个日期之间天数
    public static int calcDays(String startDate, String endDate) {
        int days = 1;
        try {
            long start = dateFormat.parse(startDate).getTime();//获取开始时间的毫秒值
            long end = dateFormat.parse(endDate).getTime();//结束时间毫秒值
            days = (int) ((end - start) / (24 * 60 * 60 * 1000));
        } catch (ParseException e) {
            e.printStackTrace();
            return -1;
        }
        return days;
    }

    //根据日期返回今天是周几
    public static int getChinaWeek(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int week = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (week == 0) {
            return 7;
        } else {
            return week;
        }
    }

    //指定某个月的第一天
    public static Date getTimeMonFirstDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(getYear(date), getMonth(date) - 1, 1);
        return c.getTime();
    }

    //指定某个月的最后一天
    public static Date getTimeMonLastDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(getYear(date), getMonth(date), 1);
        c.setTimeInMillis(c.getTimeInMillis() - (24 * 3600 * 1000));
        return c.getTime();
    }

    //返回当前月第一天
    public static Date getMonFirstDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.set(getYear(date), getMonth(date) - 1, 1);
        return c.getTime();
    }

    //返回当前月最后一天
    public static Date getMonLastDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.set(getYear(date), getMonth(date), 1);

        c.setTimeInMillis(c.getTimeInMillis() - (24 * 3600 * 1000));
        return c.getTime();
    }

    //TODO : 获取指定的 年 月 日  时 分 秒 毫秒
    //根据日期返回年
    public static int getYear(Date date) {
        if (date == null) {
            date = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);//设置时间
        return c.get(Calendar.YEAR);

    }

    //根据日期返回月
    public static int getMonth(Date date) {
        if (date == null) {
            date = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.MONTH) + 1;

    }

    //根据日期返回日
    public static int getDay(Date date) {
        if (date == null) {
            date = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.DAY_OF_MONTH);

    }

    //根据日期返回时
    public static int getHour(Date date) {
        if (date == null) {
            date = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.HOUR_OF_DAY);

    }

    //根据日期返回分
    public static int getMinute(Date date) {
        if (date == null) {
            date = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.MINUTE);

    }

    //根据日期返回秒
    public static int getSecond(Date date) {
        if (date == null) {
            date = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.SECOND);
    }

    //根据日期返回毫秒
    public static long getMillis(Date date) {
        if (date == null) {
            date = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);//使用指定日期设置时间
        return c.getTimeInMillis();
    }


    // TODO : date 转 String
    //返回格式 :  yyyy-MM-dd
    public static String formatyyyy_MM_dd(Date date) {
        return dateFormat.format(date);
    }

    //返回格式 :  yyyyMMdd
    public static String formatyyyyMMdd(Date date) {
        return yyyyMMdd.format(date);
    }

    //返回格式 :  yyyy-MM-dd HH:mm:ss
    public static String formatdataTimeFormat(Date date) {
        return dataTimeFormat.format(date);
    }

    //返回格式 :  yyyyMMddHHmmss
    public static String formatyyyyMMddHHmmss(Date date) {
        return yyyyMMddHHmmssDateTimeFormat.format(date);
    }

    //返回格式 :  yyyy年MM月dd日
    public static String formatChinese(Date date) {
        return dateFormatChinese.format(date);
    }

    //返回格式 :  yyyy年MM月dd日 HH时mm分ss秒
    public static String formatTimeChinese(Date date) {
        return dateTimeFormatChinese.format(date);
    }

    //返回格式 : yyyyMM
    public static String formatyyyyMM(Date date) {
        return dateMonthFormat.format(date);
    }

    //返回格式 : yyyy-MM
    public static String formatyyyy_MM(Date date) {
        return dateMonthFormatYYYY_MM.format(date);
    }

    //返回格式 : yyyyMMddHHmmsssss
    public static String CurrentTimeFormat(Date date){
        return CurrentTimeFormat.format(date);
    }


    //TODO : String 转 date
    //传入格式 :  yyyy-MM-dd
    public static Date parseyyyy_MM_dd(String date) {
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //传入格式 :  yyyyMMdd
    public static Date parseyyyyMMdd(String date) {
        try {
            return yyyyMMdd.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //传入格式 :  yyyy-MM-dd HH:mm:ss
    public static Date parsedataTimeFormat(String date) {
        try {
            return dataTimeFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //传入格式 :  yyyyMMddHHmmss
    public static Date parseyyyyMMddHHmmss(String date) {
        try {
            return yyyyMMddHHmmssDateTimeFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //传入格式 :  yyyy年MM月dd日
    public static Date parseChinese(String date) {
        try {
            return dateFormatChinese.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //传入格式 :  yyyy年MM月dd日 HH时mm分ss秒
    public static Date parseTimeChinese(String date) {
        try {
            return dateTimeFormatChinese.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //传入格式 : yyyyMM
    public static Date parseyyyyMM(String date) {
        try {
            return dateMonthFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //传入格式 : yyyy-MM
    public static Date parseyyyy_MM(String date) {
        try {
            return dateMonthFormatYYYY_MM.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
