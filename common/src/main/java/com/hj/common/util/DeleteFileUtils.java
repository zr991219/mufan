package com.hj.common.util;

import java.io.File;

public class DeleteFileUtils {

    /**
     * 删除本地文件
     * @param fileName
     * @return
     */
    public static String deleteFile(String fileName) {
        File file = new File(fileName);
        if (file.exists()){
            file.delete();
            return "ok";
        }
        return "no";
    }

    public static void main(String[] args) {
        System.out.println(deleteFile("F:/download/upload/20200904160900052vMkd1599206992712.jpg"));
    }
}
