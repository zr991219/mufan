package com.hj.common.pojo.dirctionary;

public enum QuartzStatusEnum {

    STOP("0","停止"), RUN("1","运行"), SUSPENDED("2","暂停"), DELETE("-1","删除");

    private final String code;
    private final String info;

    QuartzStatusEnum(String code, String info) {
        this.code = code;
        this.info = info;
    }

    /**
     * 根据code获取对应属性
     * @param code
     * @return
     */
    public static String getInfo(String code){
        for (QuartzStatusEnum quartzStatusEnum : QuartzStatusEnum.values()){
            if (quartzStatusEnum.getCode().equals(code)){
                return quartzStatusEnum.getInfo();
            }
        }
        return null;
    }

    /**
     * 根据属性获取对应状态码
     * @param info
     * @return
     */
    public static String getCode(String info){
        for (QuartzStatusEnum quartzStatusEnum : QuartzStatusEnum.values()){
            if (quartzStatusEnum.getInfo().equals(info)){
                return quartzStatusEnum.getCode();
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
}
