package com.hj.common.pojo.dirctionary;


/**
 * 用户性别状态
 */
public enum GenderEnum {
    MAN("1","男"),
    WOMAN("2","女"),
    UNKNOW("3","未知");

    private final String code;
    private final String info;


    GenderEnum(String code, String info) {
        this.code = code;
        this.info = info;
    }

    /**
     * 根据code获取对应属性
     * @param code
     * @return
     */
    public static String getInfo(String code){
        for (GenderEnum genderEnum : GenderEnum.values()){
            if (genderEnum.getCode().equals(code)){
                return genderEnum.getInfo();
            }
        }
        return null;
    }

    /**
     * 根据属性获取对应状态码
     * @param info
     * @return
     */
    public static String getCode(String info){
        for (GenderEnum genderEnum : GenderEnum.values()){
            if (genderEnum.getInfo().equals(info)){
                return genderEnum.getCode();
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
}
