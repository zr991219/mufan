package com.hj.common.constant;

import java.security.PublicKey;

/**
 * 高德地图
 */
public class AmapStatus {

    //应用key
    public static final String appKey = "d37d7f2f298541806faa58a624b824d3";
    //地理编码
    public static final String geoUrl = "https://restapi.amap.com/v3/geocode/geo";
    //逆地理编码
    public static final String regeoUrl = "https://restapi.amap.com/v3/geocode/regeo";
    //路径查询
    public static final String walkingUrl = "https://restapi.amap.com/v3/direction/walking";
    //ip查询
    public static final String ipUrl = "https://restapi.amap.com/v3/ip";
    //输入提示
    public static final String inputtipsUrl = "https://restapi.amap.com/v3/assistant/inputtips";
}
