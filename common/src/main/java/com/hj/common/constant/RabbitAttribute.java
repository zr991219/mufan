package com.hj.common.constant;


/**
 * rabbitmq的队列 交换机 routingkey
 */
public class RabbitAttribute {

    //直连交换机 : 根据消息里的路由值，将消息路由给对应队列
    public static final String DIRECT_EXCHANGE = "amqp.direct.exchange";
    //扇型交换机 : 发送给所有绑定的队列
    public static final String FANOUT_EXCHANGE = "amqp.fanout.exchange";
    //主题交换机 : 根据消息里的路由值，将消息路由给一个或多个绑定队列。
    public static final String TOPIC_EXCHANGE = "amqp.topic.exchange";
    /**
     * 头交换机 :
     *      当”x-match”为“any”时，消息头的任意一个值被匹配就可以满足条件
     *      当”x-match”设置为“all”的时候，就需要消息头的所有值都匹配成功
     */
    public static final String HEADERS_EXCHANGE = "amqp.headers.exchange";


    //TODO : 短信
    public static final String SMS_QUEUE = "sms_queue";
    public static final String SEND_SMS_ROUTINGKEY = "mufan.sms.send";

    //TODO : websocket
    public static final String WEBSOCKET_QUEUE = "websocket_queue";
    public static final String SEND_WEBSOCKET_ROUTINGKEY = "mufan.websocket.send";

    //TODO : 延时队列常量
    //延时队列
    public static final String DELAY_QUEUE = "queue_delay";
    //延时routing key
    public static final String DELAY_ROUTINGKEY = "mufan.delay";

    //TODO : 设置死信队列
    //死信交换机
    public static final String DEAD_EXCHANGE = "dead.exchange";
    //死信队列
    public static final String DEAD_QUEUE = "queue_dead";
    //死信路由
    public static final String DEAD_ROUTINGKEY = "mufan.dead";

}
