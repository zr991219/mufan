package com.hj.common.designer;

/**
 * 单例构建 StringBuilder 对象
 */
public class StrBuilder {

    //volatile防止指令重排出现
    private static volatile StringBuilder stringBuilder;

    private StrBuilder() {
    }

    public static StringBuilder getStringBuilder(){
        if (stringBuilder == null) {  // 线程A和线程B同时看到singleton = null，如果不为null，则直接返回singleton
            synchronized(StringBuilder.class) { // 线程A或线程B获得该锁进行初始化
                if (stringBuilder == null) { // 其中一个线程进入该分支，另外一个线程则不会进入该分支
                    stringBuilder = new StringBuilder();
                }
            }
        }
        return stringBuilder;
    }
}
