package com.hj.search.controller;

import com.hj.common.pojo.model.AjaxResult;
import com.hj.common.util.StringUtils;
import com.hj.search.common.pojo.Esdoc;
import com.hj.search.service.EsService;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/es")
public class EsController {

    @Autowired
    private EsService esService;

    /**
     * 创建索引
     * @param indexName 索引名称
     * @param shards 分片数
     * @param replicas 副本数
     * @return
     */
    @GetMapping("/createIndex")
    public AjaxResult createIndex(String indexName, Integer shards, Integer replicas){
//        Map<String,Map<String,String>> outside = new HashMap<>();
//        Map<String,String> name = new HashMap<>();
//        name.put("type","text");
//        outside.put("name",name);
//        esService.createIndex(indexName,shards,replicas,outside);
        esService.createIndex(indexName,shards,replicas,null);
        return AjaxResult.success("success");
    }

    /**
     * 删除索引
     * @param indexName 索引名称
     * @return
     */
    @DeleteMapping("/deleteIndex")
    public AjaxResult deleteIndex(String indexName){
        boolean flag = esService.deleteIndex(indexName);
        if (flag){
            return AjaxResult.success("success");
        }
        return AjaxResult.error("faliure");
    }

    /**
     * 添加文档
     * @param esdoc
     * @return
     */
    @PostMapping("/insertDoc")
    public AjaxResult insertDoc(Esdoc esdoc,String type,String indexName,String id){
        if (StringUtils.isEmpty(type)){
            return AjaxResult.error("type类型为空");
        }
        Map<String, Object> map = esService.insertDoc(indexName, type, id, esdoc);
        return AjaxResult.success("success",map);
    }

    /**
     * 删除文档
     * @param type
     * @param indexName
     * @param id
     * @return
     */
    @DeleteMapping("/deleteDoc")
    public AjaxResult deleteDoc(String type,String indexName,String id){
        if (StringUtils.isEmpty(type)){
            return AjaxResult.error("type类型为空");
        }
        String str = esService.deleteDoc(indexName,type,id);
        return AjaxResult.success("success",str);
    }

    /**
     * 修改文档
     * @param esdoc
     * @param type
     * @param indexName
     * @param id
     * @return
     */
    @PutMapping("/localUpdateDoc")
    public AjaxResult localUpdateDoc(Esdoc esdoc,String type,String indexName,String id){
        if (StringUtils.isEmpty(type)){
            return AjaxResult.error("type类型为空");
        }
        Map<String, Object> map = esService.localUpdateDoc(indexName, type, id, esdoc);
        return AjaxResult.success("success",map);
    }

    /**
     * 根据id搜索
     * @param indexName
     * @param type
     * @param id
     * @return
     */
    @GetMapping("/findById")
    public AjaxResult findById(String indexName, String type, String id){
        GetResponse response = esService.findById(indexName, type, id);
        return AjaxResult.success(response.getSource());
    }

    /**
     * 根据ids批量搜索
     * @param indexName
     * @param type
     * @param ids
     * @return
     */
    @GetMapping("/findByIds")
    public AjaxResult findByIds(String indexName, String type, String[] ids){
        SearchHits searchHits = esService.findByIds(indexName, type, ids);
        List<Object> list = new ArrayList<>();
        for (SearchHit searchHit : searchHits) {
            list.add(searchHit.getSourceAsMap());
        }
        return AjaxResult.success(list);
    }
}
