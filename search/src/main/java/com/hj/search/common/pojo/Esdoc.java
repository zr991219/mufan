package com.hj.search.common.pojo;

import com.hj.search.common.pojo.entity.EsEntity;
import lombok.Data;

@Data
public class Esdoc {
    private String name;
    private String remark;
    private String log;
    private String low;
    private String high;

}
