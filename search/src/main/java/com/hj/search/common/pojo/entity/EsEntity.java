package com.hj.search.common.pojo.entity;

import lombok.Data;

@Data
public class EsEntity {
    private String indexName;
    private String type;
}
