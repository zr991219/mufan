## 接口
#### 索引

* 创建索引 :  GET --> 127.0.0.1:9771/es/es/createIndex
```http request
    indexName=索引名称
    shards=分片
    replicas=副本数
```
* 删除索引 :  DELETE --> 127.0.0.1:9771/es/es/deleteIndex
```http request
    indexName=索引名称
```

#### 文档更新

* 添加或修改文档 : POST --> 127.0.0.1:9771/es/es/insertDoc
```http request
    type=类型
    indexName=索引名称
```
* 删除文档 : DELETE --> 127.0.0.1:9771/es/es/deleteDoc
```http request
    type=类型
    indexName=索引名称
    id=文档id
```
* 局部修改 : PUT -->127.0.0.1:9771/es/es/localUpdateDoc
```http request
    type=类型
    indexName=索引
    id=文档id
```

#### 文档搜索

* 根据id查询 : GET --> 127.0.0.1:9771/es/es/findById
```http request
    indexName=索引
    type=类型
    id=_id
```
* 根据ids批量查询 : GET --> 127.0.0.1:9771/es/es/findByIds
```http request
    indexName=索引
    type=类型
    ids=一个数组[_id] --- 不传ids查询全部数据 默认查询10条
```