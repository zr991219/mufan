package com.hj.gaode.common.pojo.dto;

import lombok.Data;

@Data
public class GeoDTO {

    //结构化地址信息
    private String formatted_address;
    //国家
    private String country;
    //省份
    private String province;
    //城市
    private String city;
    //城市编码
    private String citycode;
    //所在区
    private String district;
    //街道
    private String street;
    //门牌
    private String number;
    //区域编码
    private String adcode;
    //坐标点
    private String location;
    //匹配级别
    private String level;
}
