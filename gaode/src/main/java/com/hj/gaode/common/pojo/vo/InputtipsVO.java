package com.hj.gaode.common.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 搜索提示
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InputtipsVO {

    //搜索关键字
    private String keywords;
    //搜索城市
    private String city;


}
