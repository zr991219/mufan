package com.hj.gaode.controller;


import com.hj.common.pojo.model.AjaxResult;
import com.hj.common.util.StringUtils;
import com.hj.gaode.common.pojo.dto.GeoDTO;
import com.hj.gaode.common.pojo.dto.IPDTO;
import com.hj.gaode.common.pojo.dto.InputtipsDTO;
import com.hj.gaode.common.pojo.vo.GeoVO;
import com.hj.gaode.common.pojo.vo.InputtipsVO;
import com.hj.gaode.service.GaodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/amap")
public class GaodeController {

    @Autowired
    private GaodeService gaodeService;

    /**
     * 地理编码
     * @param geoVO
     * @return
     */
    @GetMapping("/geo")
    public AjaxResult geo(GeoVO geoVO) {
        if (StringUtils.isEmpty(geoVO.getAddress())) return AjaxResult.error("结构化地址信息为空");
        List<GeoDTO> geoList = gaodeService.geo(geoVO);
        return AjaxResult.success(geoList);
    }

    /**
     *  路径查询
     * @param origin
     * @param destination
     * @return
     */
    @GetMapping("/walking")
    public AjaxResult walking(String origin,String destination){
        String begin = gaodeService.geo(origin);
        String end = gaodeService.geo(destination);
        gaodeService.walking(begin,end);
        return AjaxResult.success();
    }

    /**
     * 根据ip定位 不传默认使用当前位置
     * @param ip
     * @return
     */
    @GetMapping("/ip")
    public AjaxResult ip(String ip){
        IPDTO ipdto = gaodeService.IPposition(ip);
        return AjaxResult.success(ipdto);
    }

    /**
     * 搜索提示
     * @param inputtipsVO
     * @return
     */
    @GetMapping("/inputtips")
    public AjaxResult inputtips(InputtipsVO inputtipsVO) {
        if (StringUtils.isEmpty(inputtipsVO.getKeywords())) return AjaxResult.error("搜索关键字为空");
        List<InputtipsDTO> inputtips = gaodeService.inputtips(inputtipsVO);
        return AjaxResult.success(inputtips);
    }
}
