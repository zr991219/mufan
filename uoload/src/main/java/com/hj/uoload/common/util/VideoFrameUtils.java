package com.hj.uoload.common.util;

import com.hj.common.exception.BusinessException;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * 获取视频第一帧
 */
public class VideoFrameUtils {
    /**
     *
     * @param videofile : 视频路径  C:\Users\dongdong\Desktop\压缩片\test\image\video.mp4
     * @param framefile : 图片存储路径 C:\Users\dongdong\Desktop\压缩片\test\image\image.jpg
     * @throws Exception
     */
    public static void fetchFrame(String videofile, String framefile) {
        File targetFile = new File(framefile);
        try {
            FFmpegFrameGrabber ff = new FFmpegFrameGrabber(videofile);
            ff.start();
            //获取所有图片
            int lenght = ff.getLengthInFrames();
            int i = 0;
            Frame f = null;
            while (i < lenght) {
                // 过滤前5帧，避免出现全黑的图片，依自己情况而定
                f = ff.grabFrame();
                if ((i > 5) && (f.image != null)) {
                    break;
                }
                i++;
            }
            opencv_core.IplImage img = f.image;
            int owidth = img.width();
            int oheight = img.height();
            // 对截取的帧进行等比例缩放
            int width = 800;
            int height = (int) (((double) width / owidth) * oheight);
            BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
            bi.getGraphics().drawImage(f.image.getBufferedImage().getScaledInstance(width, height, Image.SCALE_SMOOTH),
                    0, 0, null);
            ImageIO.write(bi, "jpg", targetFile);
            //ff.flush();
            ff.stop();
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("视频第一帧截取失败");
        }

    }

}
