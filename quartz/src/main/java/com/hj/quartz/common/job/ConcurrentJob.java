package com.hj.quartz.common.job;


import com.hj.common.exception.BusinessException;
import com.hj.quartz.common.constants.ScheduleConstants;
import com.hj.quartz.common.pojo.entity.JobQuartz;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 允许并发执行多个job
 */

public class ConcurrentJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //获取运行参数
        JobQuartz jobQuartz = (JobQuartz) jobExecutionContext.getMergedJobDataMap().get(ScheduleConstants.TASK_PROPERTIES);
        //执行方法
        try {
            JobInvoke.invokeMethod(jobQuartz);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("job任务执行失败");
        }
    }
}
