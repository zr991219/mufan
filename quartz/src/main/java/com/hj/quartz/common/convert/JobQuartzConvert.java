package com.hj.quartz.common.convert;

import com.hj.quartz.common.pojo.dto.JobQuartzDTO;
import com.hj.quartz.common.pojo.entity.JobQuartz;
import com.hj.quartz.common.pojo.vo.JobQuartzVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface JobQuartzConvert {

    //转换类实例
    JobQuartzConvert INSTANCE = Mappers.getMapper(JobQuartzConvert.class);

    //entityList转DtoList
    List<JobQuartzDTO> entityList2DtoList(List<JobQuartz> jobQuartzList);

    //vo转entity
    JobQuartz vo2entity(JobQuartzVO jobQuartzVO);
}
