package com.hj.quartz.common.job;


import com.hj.common.exception.BusinessException;
import com.hj.common.util.ServletUtils;
import com.hj.quartz.common.constants.ScheduleConstants;
import com.hj.quartz.common.pojo.entity.JobQuartz;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 禁止并发执行多个相同的job
 *
 * 例如 : 一个Job类,叫做SayHelloJob, 并在这个Job上加了这个注解,
 *       然后在这个Job上定义了很多个JobDetail, 如sayHelloToJoeJobDetail, sayHelloToMikeJobDetail,
 *       那么当scheduler启动时, 不会并发执行多个sayHelloToJoeJobDetail或者sayHelloToMikeJobDetail,
 *       但可以同时执行sayHelloToJoeJobDetail跟sayHelloToMikeJobDetail
 */
@DisallowConcurrentExecution//禁止并发执行多个相同的job
public class BanConcurrentJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //获取运行参数
        JobQuartz jobQuartz = (JobQuartz) jobExecutionContext.getMergedJobDataMap().get(ScheduleConstants.TASK_PROPERTIES);
        //执行方法
        try {
            JobInvoke.invokeMethod(jobQuartz);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("job任务执行失败");
        }
    }
}
