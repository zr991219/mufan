package com.hj.quartz.controller;


import com.hj.auth.common.util.JwtTokenUtil;
import com.hj.common.pojo.model.AjaxResult;
import com.hj.common.util.DateUtils;
import com.hj.common.util.StringUtils;
import com.hj.quartz.common.convert.JobQuartzConvert;
import com.hj.quartz.common.pojo.dto.JobQuartzDTO;
import com.hj.quartz.common.pojo.entity.JobQuartz;
import com.hj.quartz.common.pojo.vo.JobQuartzVO;
import com.hj.quartz.common.util.CronUtils;
import com.hj.quartz.service.JobQuartzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/JobQuartz")
public class JobQuartzController {

    @Autowired
    private JobQuartzService jobQuartzService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     * 查询所有定时任务
     * @return
     */
    @GetMapping("/selectList")
    public List<JobQuartzDTO> selectList(){
        List<JobQuartz> quartzList = jobQuartzService.selectList();
        List<JobQuartzDTO> jobQuartzDTOList = JobQuartzConvert.INSTANCE.entityList2DtoList(quartzList);
        return jobQuartzDTOList;
    }

    /**
     * 创建定时任务
     * @param jobQuartzVO
     * @return
     */
    @PostMapping("/createJob")
    public AjaxResult createJob(@RequestBody JobQuartzVO jobQuartzVO) {
        if (!CronUtils.isValid(jobQuartzVO.getCronExpression())) {
            return AjaxResult.error("cron表达式不正确");
        }
        //校验类名首字母是否小写
        if (StringUtils.isFirstLetter(jobQuartzVO.getBeanClass())){
            return AjaxResult.error("执行方法类名首字母需要小写");
        }
        JobQuartz jobQuartz = JobQuartzConvert.INSTANCE.vo2entity(jobQuartzVO);
        jobQuartz.setCreator(jwtTokenUtil.getUserName());//设置用户名
        int i = jobQuartzService.createJob(jobQuartz);
        return AjaxResult.success("创建成功");
    }

    /**
     * 删除定时任务
     * @param jobIds
     * @return
     */
    @DeleteMapping("/delJob/{jobIds}")
    public AjaxResult delJob(@PathVariable("jobIds") List<Integer> jobIds) {
        jobQuartzService.deleteByJobIds(jobIds);
        return AjaxResult.success("删除成功");
    }

    /**
     * 修改定时任务
     * @param jobQuartzVO
     * @return
     */
    @PutMapping("/updateJob")
    public AjaxResult updateJob(@RequestBody JobQuartzVO jobQuartzVO) {
        if (StringUtils.isNotEmpty(jobQuartzVO.getCronExpression())) {
            if (!CronUtils.isValid(jobQuartzVO.getCronExpression())) {
                return AjaxResult.error("cron表达式不正确");
            }
        }
        JobQuartz jobQuartz = JobQuartzConvert.INSTANCE.vo2entity(jobQuartzVO);
        jobQuartzService.updateJob(jobQuartz);
        return AjaxResult.success("修改成功");
    }



    //TODO : 定时任务执行测试方法
    @GetMapping("/test")
    public void test(){
        System.out.println("测试 : hello quartz !!! ==== 执行时间 : " + DateUtils.getCurrentTime());
    }

    @GetMapping("/test2")
    public void test2(Integer code){
        System.out.println("测试 : hello quartz !!! ==== 执行时间 : " + DateUtils.getCurrentTime() + " ==== 入参 code : " + code);
    }
}
