package com.hj.auth.common.pojo;


import com.hj.auth.common.pojo.dto.UserDTO;
import com.hj.auth.common.pojo.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class LoginUser implements UserDetails {

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 登陆时间
     */
    private Long loginTime;

    /**
     * 过期时间
     */
    private Long expireTime;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 权限列表
     */
    private List<String> permissions;

    /**
     * 用户信息
     */
    private UserDTO userDTO;


    //TODO : 构造方法
    public LoginUser() {
    }

    public LoginUser(UserDTO user) {
        this.userDTO = user;
    }

    public LoginUser(List<String> permissions, UserDTO user) {
        this.permissions = permissions;
        this.userDTO = user;
    }


    //TODO : 系统属性
    //密码
    @Override
    public String getPassword() {
        return userDTO.getPassWord();
    }

    //用户名
    @Override
    public String getUsername() {
        return userDTO.getUserName();
    }

    //权限信息
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    //账户是否过期  true : 没有  false : 过期
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    //账户是否锁定<是否冻结> false : 冻结
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    //密码是否过期  true : 没有  false : 过期
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    //账户是否可用<用户是否删除>
    @Override
    public boolean isEnabled() {
        return true;
    }


    //TODO : get/set方法
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Long loginTime) {
        this.loginTime = loginTime;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    public UserDTO getUser() {
        return userDTO;
    }

    public void setUser(UserDTO user) {
        this.userDTO = user;
    }
}
