package com.hj.auth.common.pojo.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用于返回用户信息
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
@Data
public class UserDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Integer userId;

    //用户名
    private String userName;

    //密码
    private String passWord;

    //用户头像
    private String userIco;

    //0 男 1 女 3 未知
    private String userSex;

    //状态  0正常，1删除，2黑名单
    private String userStatus;

    //手机号
    private String userPhone;

    //创建时间
    private Date createTime;

    //类型
    private String userType;

    //userid为 1 的用户是超级管理员
    public boolean isAdmin(){
        return this.userId != null && 1 == this.userId;
    }
}