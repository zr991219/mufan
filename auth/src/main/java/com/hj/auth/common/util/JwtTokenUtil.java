package com.hj.auth.common.util;

import com.hj.auth.common.pojo.LoginUser;
import com.hj.common.constant.Constants;
import com.hj.common.util.IPUtils;
import com.hj.common.util.ServletUtils;
import com.hj.common.util.StringUtils;
import com.hj.common.util.UUIDUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * jwt令牌
 */
@Component
public class JwtTokenUtil {

	//token超时时间
    @Value("${jwt.expiration}")
    public long expiration;

    //生成token的秘钥
    @Value("${jwt.secret}")
    public String secret;

    //前端传入token的key值
    @Value("${jwt.header}")
    public String header;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 获取用户身份信息
     *
     * @return 用户信息
     */
    public LoginUser getLoginUser(HttpServletRequest request) {
        // 获取请求携带的令牌
        String token = getToken(request);
        if (StringUtils.isNotEmpty(token)) {
            String userKey = getTokenUuid(token,true);
            LoginUser user = (LoginUser)redisTemplate.opsForValue().get(userKey);
            return user;
        }
        return null;
    }

    public String getTokenUuid(){
        return getTokenUuid(getToken(ServletUtils.getRequest()),false);
    }

    /**
     * 获取token里的uuid
     */
    public String getTokenUuid(String token,boolean flag){
        Claims claims = parseToken(token);
        String uuid = (String) claims.get(Constants.LOGIN_USER_KEY);
        String userKey = getTokenKey(uuid);
        String key = flag ? userKey : uuid;
        return key;
    }

    /**
     * 创建令牌  并判断该用户是否已经登陆  如果已登陆 则删除用户旧的token信息
     *
     * @param loginUser 用户信息
     * @return 令牌
     */
    public String createToken(LoginUser loginUser) {
        //作为用户信息存储在redis的key
        String token = UUIDUtils.getUuid();
        loginUser.setToken(token);

        setUserTokenKey(loginUser.getUser().getUserId(),token);
        //刷新令牌 : 把用户数据存储到redis 默认 30分钟过期  用户数据过期后 则token随之过期
        refreshToken(loginUser);
        //创建令牌
        Map<String, Object> claims = new HashMap<>();
        claims.put(Constants.LOGIN_USER_KEY, token);
        return createToken(claims);
    }


    /**
     * 根据userid判断token在redis是否存在key 存在则删除后 在创建token信息并存储用户信息
     * @param userId
     * @return
     */
    public void setUserTokenKey(Integer userId,String token) {
        String tokenUserId = Constants.TOKEN_USER_ID + userId;
        //根据userid查询token
        String oderToken = (String) redisTemplate.opsForValue().get(tokenUserId);
        String userKey = getTokenKey(oderToken);
        //根据token查询用户信息
        LoginUser loginUser = (LoginUser) redisTemplate.opsForValue().get(userKey);
        //判断用户信息是否存在  存在则删除用户信息
        if (StringUtils.isNotNull(loginUser)) {
            delLoginUser(oderToken);
        }
        redisTemplate.opsForValue().set(tokenUserId,token,expiration,TimeUnit.MINUTES);
    }


    /**
     * 验证令牌有效期，相差不足20分钟，自动刷新缓存
     *
     * @param loginUser
     * @return 令牌
     */
    public void verifyToken(LoginUser loginUser) {
        long expireTime = loginUser.getExpireTime();
        long currentTime = System.currentTimeMillis();
        if (expireTime - currentTime <= 20 * 60 * 1000L) {
            refreshToken(loginUser);
        }
    }

    /**
     * 刷新令牌有效期
     * 设置令牌过期时间 并把用户信息放到redis里
     * @param loginUser 登录信息
     */
    public void refreshToken(LoginUser loginUser) {
        loginUser.setLoginTime(System.currentTimeMillis());//获取系统当前毫秒值 设置为登陆时间
        loginUser.setExpireTime(loginUser.getLoginTime() + expiration * 60 * 1000);//设置过期时间为yaml文件指定时间
        loginUser.setIpaddr(IPUtils.getIpAddr(ServletUtils.getRequest()));//获取用户登陆ip
        // 根据uuid将loginUser缓存
        String userKey = getTokenKey(loginUser.getToken());
        redisTemplate.opsForValue().set(userKey, loginUser, expiration, TimeUnit.MINUTES);//把用户信息设置到redis并设置过期时间为yaml指定时间
    }

    /**
     * 拼接存储redis的key  : login_tokens + uuid
     * @param uuid
     * @return
     */
    private String getTokenKey(String uuid) {
        return Constants.LOGIN_TOKEN_KEY + uuid;
    }

    /**
     * 从数据声明生成令牌
     *
     * @param claims 数据声明
     * @return 令牌
     */
    private String createToken(Map<String, Object> claims) {
        String token = Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret).compact();
        return token;
    }

    /**
     * 从令牌中获取数据声明
     *
     * @param token 令牌
     * @return 数据声明
     */
    private Claims parseToken(String token) {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * 获取请求token
     *
     * @param request
     * @return token
     */
    public String getToken(HttpServletRequest request) {
        String token = request.getHeader(header);
        if (StringUtils.isNotEmpty(token) && token.startsWith(Constants.TOKEN_PREFIX)) {
            //如果token为null 或 以Bearer开头  则把token设置为空串 ""
            token = token.replace(Constants.TOKEN_PREFIX, "");
        }
        return token;
    }

    /**
     * 设置用户身份信息
     */
    public void setLoginUser(LoginUser loginUser) {
        if (StringUtils.isNotNull(loginUser) && StringUtils.isNotEmpty(loginUser.getToken())) {
            refreshToken(loginUser);
        }
    }

    /**
     * 删除用户身份信息
     */
    public void delLoginUser(String token) {
        if (StringUtils.isNotEmpty(token)) {
            String userKey = getTokenKey(token);
            redisTemplate.delete(userKey);
        }
    }

    //获取用户名
    public String getUserName(){
       return getLoginUser(ServletUtils.getRequest()).getUser().getUserName();
    }
}
