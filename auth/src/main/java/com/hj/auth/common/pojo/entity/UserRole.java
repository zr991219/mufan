package com.hj.auth.common.pojo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 表 user_role
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
@Data
public class UserRole implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Integer roleId;

    //
    private Integer userId;

}