package com.hj.auth.mapper;

import com.hj.auth.common.pojo.entity.Menu;
import org.springframework.stereotype.Repository;
import java.util.List;


/**
 * 菜单权限表 数据层
 *
 * @author d
 * @date 2020-08-31 08:50:52
 */
@Repository
public interface MenuMapper {


    //根据userid查询用户权限目录
    List<String> selectMenuByUserId(Integer userId);

    //查询菜单权限表信息
    public Menu selectMenuById(Integer menuId);

    // 查询菜单权限表列表
    public List<Menu> selectMenuList(Menu menu);

    // 新增菜单权限表
    public int insertMenu(Menu menu);

    //修改菜单权限表
    public int updateMenu(Menu menu);

    //删除菜单权限表
    public int deleteMenuById(Integer menuId);

    // 批量删除菜单权限表
    public int deleteMenuByIds(String[] menuIds);

}