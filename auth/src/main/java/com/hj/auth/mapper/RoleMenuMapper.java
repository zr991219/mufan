package com.hj.auth.mapper;

import com.hj.auth.common.pojo.entity.RoleMenu;
import org.springframework.stereotype.Repository;
import java.util.List;


/**
 * 角色 权限中间表 数据层
 *
 * @author d
 * @date 2020-08-31 08:50:52
 */
@Repository
public interface RoleMenuMapper {

    //查询角色 权限中间表信息
    public RoleMenu selectRoleMenuById(Integer roleId);

    // 查询角色 权限中间表列表
    public List<RoleMenu> selectRoleMenuList(RoleMenu roleMenu);

    // 新增角色 权限中间表
    public int insertRoleMenu(RoleMenu roleMenu);

    //修改角色 权限中间表
    public int updateRoleMenu(RoleMenu roleMenu);

    //删除角色 权限中间表
    public int deleteRoleMenuById(Integer roleId);

    // 批量删除角色 权限中间表
    public int deleteRoleMenuByIds(String[] roleIds);
}