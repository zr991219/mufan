package com.hj.auth.mapper;


import com.hj.auth.common.pojo.entity.UserRole;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  数据层
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
@Repository
public interface UserRoleMapper {

    //查询信息
    public UserRole selectUserRoleById(Integer roleId);

    // 查询列表
    public List<UserRole> selectUserRoleList(UserRole userRole);

    // 新增
    public int insertUserRole(UserRole userRole);

    //修改
    public int updateUserRole(UserRole userRole);

    //删除
    public int deleteUserRoleById(Integer roleId);

    // 批量删除
    public int deleteUserRoleByIds(String[] roleIds);
}