package com.hj.auth.service.websecurity;

import com.hj.auth.common.pojo.LoginUser;
import com.hj.auth.common.util.JwtTokenUtil;
import com.hj.common.util.ServletUtils;
import com.hj.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 自定义权限校验 : 可以取redis用户信息里的权限
 */
@Service("per")
public class Permission {

    /**
     * 所有权限标识
     */
    private static final String ALL_PERMISSION = "*:*:*";

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     * 验证用户是否具备某个权限
     *
     * @param permission
     * @return
     */
    public boolean hasPermission(String permission) {
        if (StringUtils.isEmpty(permission)) {
            return false;
        }
        LoginUser loginUser = jwtTokenUtil.getLoginUser(ServletUtils.getRequest());
        //如果用户信息为空 或者 权限为空 则校验失败
        if (StringUtils.isNull(loginUser) || StringUtils.isEmpty(loginUser.getPermissions())){
            return false;
        }
        return isPermission(loginUser.getPermissions(),permission);
    }


    /**
     * 校验权限
     * @param permissions  : 用户权限列表
     * @param permission   : 访问的方法所需要的权限
     * @return
     */
    private boolean isPermission(List<String> permissions,String permission){
        //contains : 其中包含指定值时 返回true
        return permissions.contains(ALL_PERMISSION) || permissions.contains(StringUtils.trim(permission));
    }

}
