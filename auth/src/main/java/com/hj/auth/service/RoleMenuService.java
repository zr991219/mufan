package com.hj.auth.service;


import com.hj.auth.common.pojo.entity.RoleMenu;

import java.util.List;

/**
 * 角色 权限中间表 服务层
 *
 * @author d
 * @date 2020-08-31 08:50:52
 */
public interface RoleMenuService {

    //查询角色 权限中间表信息
    public RoleMenu selectRoleMenuById(Integer roleId);

    //查询角色 权限中间表列表
    public List<RoleMenu> selectRoleMenuList(RoleMenu roleMenu);

    //新增角色 权限中间表
    public int insertRoleMenu(RoleMenu roleMenu);

    //修改角色 权限中间表
    public int updateRoleMenu(RoleMenu roleMenu);

    //删除角色 权限中间表信息
    public int deleteRoleMenuByIds(String ids);
}