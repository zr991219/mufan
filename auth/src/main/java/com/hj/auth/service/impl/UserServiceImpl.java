package com.hj.auth.service.impl;


import com.hj.auth.common.convert.UserConvert;
import com.hj.auth.common.pojo.LoginUser;
import com.hj.auth.common.pojo.entity.User;
import com.hj.auth.common.util.JwtTokenUtil;
import com.hj.auth.mapper.UserMapper;
import com.hj.auth.service.UserService;
import com.hj.common.exception.BusinessException;
import com.hj.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户表 服务层实现
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private PasswordEncoder passwordEncoder;


    /**
     * 校验用户信息 返回token
     *
     * @return
     */
    @Override
    public String login(String userName, String passWord) {
        // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
        Authentication authentication = null;
        try {
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, passWord));
        } catch (Exception e) {
            if (e instanceof BadCredentialsException) {
                throw new BusinessException("用户名或密码错误");
            }else{
                throw e;
            }
        }
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        return jwtTokenUtil.createToken(loginUser);
    }


    //查询用户表信息
    @Override
    public User selectUserById(Integer userId) {
        return userMapper.selectUserById(userId);
    }

    // 查询用户表列表
    @Override
    public List<User> selectUserList(User user) {
        return userMapper.selectUserList(user);
    }

    // 新增用户表
    @Override
    public int insertUser(User user) {
        return userMapper.insertUser(user);
    }

    //修改用户表
    @Override
    public int updateUser(User user) {
        //密码非空  加密
        if (StringUtils.isNotEmpty(user.getPassWord())){
            user.setPassWord(passwordEncoder.encode(user.getPassWord()));
        }
        int i = userMapper.updateUser(user);
        //更新redis用户信息
        LoginUser loginUser = new LoginUser();
        loginUser.setToken(jwtTokenUtil.getTokenUuid());
        loginUser.setUser(UserConvert.INSTANCE.entity2dot(userMapper.selectUserByUserName(user.getUserName())));
        jwtTokenUtil.refreshToken(loginUser);
        return i;
    }

    //删除用户表对象
    @Override
    public int deleteUserByIds(String ids) {
        return userMapper.deleteUserByIds(ids.split(","));
    }
}