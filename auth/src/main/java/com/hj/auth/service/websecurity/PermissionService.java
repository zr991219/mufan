package com.hj.auth.service.websecurity;

import com.hj.auth.common.pojo.dto.UserDTO;
import com.hj.auth.service.MenuService;
import com.hj.auth.service.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 查询用户权限
 */
@Service
public class PermissionService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuService menuService;

    /**
     * 查询用户目录权限
     * @param userDTO
     * @return
     */
    public List<String> getMenuPermission(UserDTO userDTO){
        List<String> permission = new ArrayList<>();
        if (userDTO.isAdmin()){
            permission.add("*:*:*");
        }else {
            List<String> per = menuService.selectMenuByUserId(userDTO.getUserId());
            logger.info("用户目录权限有 : {}",per);
            permission.addAll(per);
        }
        return permission;
    }

}
