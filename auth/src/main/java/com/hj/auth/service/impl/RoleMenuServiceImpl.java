package com.hj.auth.service.impl;

import com.hj.auth.common.pojo.entity.RoleMenu;
import com.hj.auth.mapper.RoleMenuMapper;
import com.hj.auth.service.RoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * 角色 权限中间表 服务层实现
 *
 * @author d
 * @date 2020-08-31 08:50:52
 */
@Service
public class RoleMenuServiceImpl implements RoleMenuService {

    @Autowired
    private RoleMenuMapper roleMenuMapper;

    //查询角色 权限中间表信息
    @Override
    public RoleMenu selectRoleMenuById(Integer roleId) {
        return roleMenuMapper.selectRoleMenuById(roleId);
    }

    // 查询角色 权限中间表列表
    @Override
    public List<RoleMenu> selectRoleMenuList(RoleMenu roleMenu) {
        return roleMenuMapper.selectRoleMenuList(roleMenu);
    }

    // 新增角色 权限中间表
    @Override
    public int insertRoleMenu(RoleMenu roleMenu) {
        return roleMenuMapper.insertRoleMenu(roleMenu);
    }

    //修改角色 权限中间表
    @Override
    public int updateRoleMenu(RoleMenu roleMenu) {
        return roleMenuMapper.updateRoleMenu(roleMenu);
    }

    //删除角色 权限中间表对象
    @Override
    public int deleteRoleMenuByIds(String ids) {
        return roleMenuMapper.deleteRoleMenuByIds(ids.split(","));
    }
}