# auth 模块介绍
* 本模块集成security + jwt实现权限控制
* 基于redis实现token过期机制
* 前端访问登陆接口 获取token时 把用户信息存放到redis里 并设置过期时间 当redis用户信息过期 则token随之失效 (*token里存放有用户信息在redis里的key值*)

### 1.实体类使用

* vo      -->  前端传参 
* entity  -->  和数据库字段对应
* dto     -->  返回数据使用
* **实体类之间使用mapstruct实现数据copy**

### 2.通过token实现一个账号只能在一个设备登陆
* 上面已介绍 在用户登陆成功后 会把token作为key 用户信息为值存储在redis里  
    * 作用 : 通过token查询用户信息
* 在次基础上 用户登陆成功后 把用户id作为key token为值 存储在redis里
    * 作用 : 通过userid查询token信息  
* 注意 : 两个key值过期时间一样
* 在如上准备后  实现逻辑如下
    * 用户在登陆时  通过userid查询到token 去查询搞token是否作为key存储有用户信息  有什么用户已经登陆  否则没有登陆
    * 有则根据查询到的token 删除对应的用户信息  然后在登陆  颁发新的token  把新的token存储在userid里
        * 此时 把用户上一次登陆的信息删除  则上一回登陆的token就失效了<该账号就会退出登录>
