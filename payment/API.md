## 支付宝接口
* 本项目为测试方便全部使用的GET请求 大家可以根据自己需要修改请求方式

* 请求参数
```http request
    outTradeNo : 订单号
    tradeNo : 交易号
    flag : 0 : 全额退款  1 : 部分退款
    authCode : 授权码
    scene : 支付场景，条码：bar_code，声波：wave_code
    storeId : 商户门店编号
    amount : 订单金额
```

* 接口
    * 支付宝电脑网站 --> 127.0.0.1:9771/payment/alipay/PCPay
    * 支付宝app  -->  127.0.0.1:9771/payment/alipay/AppPay
    * 支付宝手机网站  -->  127.0.0.1:9771/payment/alipay/WapPay
    * 支付宝条形码  -->  127.0.0.1:9771/payment/alipay/tradePay
    * 支付宝扫码支付  -->  127.0.0.1:9771/payment/alipay/tradePrecreatePay
    * 支付宝退款  -->  127.0.0.1:9771/payment/alipay/tradeRefund
    * 支付宝订单查询  -->  127.0.0.1:9771/payment/alipay/tradeQuery