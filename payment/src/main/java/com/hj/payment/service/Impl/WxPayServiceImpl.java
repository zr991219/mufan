package com.hj.payment.service.Impl;


import com.hj.common.util.IPUtils;
import com.hj.common.util.ServletUtils;
import com.hj.common.util.UUIDUtils;
import com.hj.payment.common.properties.WxPayProperties;
import com.hj.payment.service.WxPayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class WxPayServiceImpl implements WxPayService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private WxPayProperties wxPayProperties;

    @Override
    public void WapPay(String total_fee) {
        logger.info("金额 :{}",total_fee);
        Map<String, Object> m = new HashMap<>();
        m.put("appid",wxPayProperties.getAppId());//应用id
        m.put("mchid",wxPayProperties.getMchId());//商户号
        m.put("description","测试");//产品描述
        String out_trade_no = UUIDUtils.getRandomNumber(11);
        m.put("out_trade_no",out_trade_no);//商户订单号
        m.put("attach","");//附加数据
        m.put("notify_url","");//回调通知地址
        m.put("total_fee",total_fee);//金额
        String IP = IPUtils.getIpAddr(ServletUtils.getRequest());
        m.put("spbill_create_ip",IP);//生成订单IP地址
        m.put("trade_type", "MWEB");//支付方式 H5支付

    }

}
