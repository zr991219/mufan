package com.hj.payment.service;


import com.hj.payment.common.pojo.AliPayRefundVO;
import com.hj.payment.common.pojo.AliPayVO;
import com.hj.payment.common.pojo.AliTradePayVO;
import org.springframework.stereotype.Service;

public interface WxPayService {

    void WapPay(String total_fee);

}
