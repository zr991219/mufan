package com.hj.payment.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "pay.wxpay")
public class WxPayProperties {

    private String appId;
    //
    private String mchId;
    //
    private String key;
    //回调地址
    private String returnUrl;
    //异步回调
    private String notyfyUrl;
}
