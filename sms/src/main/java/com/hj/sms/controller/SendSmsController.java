package com.hj.sms.controller;


import com.hj.common.constant.RabbitAttribute;
import com.hj.common.pojo.model.AjaxResult;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendSmsController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("/sendSms")
    public AjaxResult sendSms(String phone){
        //给固定交换机发送消息 并携带路由key
        rabbitTemplate.convertAndSend(RabbitAttribute.DIRECT_EXCHANGE,RabbitAttribute.SEND_SMS_ROUTINGKEY,phone);
        return AjaxResult.success("发送成功");
    }
}
